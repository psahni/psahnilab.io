---
title: Markdown page example
---

# Markdown page example

You don't need React to write simple standalone pages.

```ruby
    def test
     puts "Hello World"
    end
```

```javascript
 console.log("Hello");
 function sayHello() {}
```
